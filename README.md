# Docker container for verilog

Container for testing designs using a specific set of open-source tools.
These tools include:
- Meson (0.57.1)
- Verilator (4.200 rev v4.200-16-gf0d66453)
- Criterion
- Yosys (0.9+4052)
- SymbiYosys
- Yices 2.6.2
