FROM ubuntu:18.04
# 18.04 because of criterion ppa
#   building criterion from source is too painful (yes, even worse than yosys and verilator)
#   verilator is built from source because the version in the ubuntu repositories is grossly out-of-date

RUN apt-get update && apt-get install -y software-properties-common

# yosys + symbiyosys dependencies
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y build-essential clang bison flex libreadline-dev gawk tcl-dev libffi-dev git mercurial graphviz xdot pkg-config python python3 libftdi-dev gperf libboost-program-options-dev autoconf libgmp-dev cmake ninja-build

# installing meson build system (more up-to-date? Ubuntu's package doesn't seem to recognize compile -C dir)
RUN apt-get install -y python3-pip && pip3 install meson
# installing verilator (an up-to-date version)
RUN git clone https://github.com/verilator/verilator verilator \
 && cd verilator \
 && git checkout stable \
 && autoconf && ./configure \
 && make -j$(nproc) \
 && make install \
 && cd ..
# installing criterion
RUN add-apt-repository ppa:snaipewastaken/ppa && apt-get update && apt-get install -y criterion-dev

# installing yosys
RUN git clone https://github.com/YosysHQ/yosys.git yosys \
 && cd yosys \
 && make -j$(nproc) \
 && make install \
 && cd ..
# installing symbiyosys
RUN git clone https://github.com/YosysHQ/SymbiYosys.git symbiyosys \
 && cd symbiyosys \
 && make install \
 && cd ..
# installing yices smt-solver
RUN add-apt-repository ppa:sri-csl/formal-methods && apt-get update && apt-get install -y yices2-dev
